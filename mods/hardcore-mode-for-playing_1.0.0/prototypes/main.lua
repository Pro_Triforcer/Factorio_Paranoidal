flib = require("__flib__.data-util")
require("__automated-utility-protocol__.util.main")
require("resources")
require("items")
require("entities")
require("recipes")
require("technologies")
require("crash-site-spaceship-entries")
flib = nil
