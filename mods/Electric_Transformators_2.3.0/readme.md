We add 5 tiers of Electric Transformators for better control over the electric supply network. The transfer power for each tier is 1MW, 10MW, 100MW, 1GW, 10GW with efficiency of ~98%

#### Use examples

* limit electric production or consumption for parts of the factory
* change electric network priorities
* maximize electric energy production while keeping accumulators charged
* stage supply priorities with a transformator grid
* keep nuclear reactors at maximum production
* and more
* tell us how transformators are used at your factory!


# Startup settings
------------------------------------------------------------------------------------------------
* efficiency: efficiency of power conversion (as percentage); minimum: 1, maximum 100, default: 96
* check for electric shorts: interval of the check for electric shorts (as ticks); disable: 0, default: 600
* hide alt key overlay: hide the blue arrows and yellow thunderbolts on placed transformators (as boolean); enable: on, default: off
* transformator hum volume: set the volume of the transformator hum; minimum 0, maximum 1.0, default 0.3

# Supported mods
------------------------------------------------------------------------------------------------
* [Power Combinator](https://mods.factorio.com/mod/power-combinator)
* [Capacity Combinator](https://mods.factorio.com/mod/capacity-combinator)
* [Transformers](https://mods.factorio.com/mod/Transformers) (legacy migration support, check the [«FAQ»](https://mods.factorio.com/mod/Electric_Transformators/faq))


# Documentation
------------------------------------------------------------------------------------------------
* read the [«FAQ»](https://mods.factorio.com/mod/Electric_Transformators/faq) and the [«Changelog»](https://mods.factorio.com/mod/Electric_Transformators/changelog) for more details
* discuss with us on the [«Discussion»](https://mods.factorio.com/mod/Electric_Transformators/discussion) page


# Known issues
-----------------------------------------------------------------------------------------------
* the transformator graphic looks otherworldly in terms of factorio design; we're looking for a graphics guru, who would like to remake it


# Compatibility
------------------------------------------------------------------------------------------------
* mods which change power pole connection areas can make Electric Transformators non-functional
* please report on the [«Discussion»](https://mods.factorio.com/mod/Electric_Transformators/discussion) page if any incompatibilities are discovered


# License
-----------------------------------------------------------------------------------------------
The mod ‹Electric Transformators› was made by max2344, OwnlyMe and others, published under the MIT license.


# Credits
-----------------------------------------------------------------------------------------------
Original idea and implementation by Monty, updated by Penguin, Dexter_Morgan, OwnlyMe, max2344.


# Notes
-----------------------------------------------------------------------------------------------
The information on this mod page represents the state of the current release and might be updated without prior notification or public announcement. Please refer to the «Documentation» section of this page for details.
