

require("prototypes.constants")
require("prototypes.settings")
require("prototypes.signals")
require("prototypes.technologies")
require("prototypes.items")
require("prototypes.recipes")
require("prototypes.entities")
require("prototypes.connections")

