

data:extend({
	{
		type = "virtual-signal",
		name = "electric-transformators-short-circuit",
		icons = {{ 
			icon = TRAFOS_GRAPHICS_BASE.."/icons/flash.png",
			tint = {r=1, g=1, b=0},
		}},
		icon_size = 32,
	},

})

