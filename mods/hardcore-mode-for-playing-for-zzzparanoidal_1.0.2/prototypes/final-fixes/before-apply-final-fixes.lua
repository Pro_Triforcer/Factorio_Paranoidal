flib = require("__flib__.data-util")
require("prototypes.final-fixes.from-update.items")
require("prototypes.final-fixes.from-update.guns")
require("prototypes.final-fixes.from-update.entities")
require("prototypes.final-fixes.from-update.recipes")
require("prototypes.final-fixes.from-update.technologies")
flib = nil
